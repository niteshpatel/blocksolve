#include <iostream.h>  
	
const short HEIGHT = 9;
const short WIDTH = 14; 

short COMPLEXITY = 6;
short temp = 0;

void reset_grid(int [][WIDTH]);
void shift_down(int [][WIDTH], int, int);
void shift_right(int [][WIDTH], int);
void shift(int [][WIDTH], int [][WIDTH]);
void display(int [][WIDTH], int);
void copy(int [][WIDTH], int [][WIDTH]);
void input(int [][WIDTH]);
void check_chain(int [][WIDTH], int [][WIDTH], int, int);

float evaluate_grid(int [][WIDTH]);
float special(int [][WIDTH], int);

bool check_limit(int a, int b);

int messenger(int [][WIDTH], int total);
int suggest(int [][WIDTH], int total);
int make_move(int [][WIDTH], int, int);
int count(int [][WIDTH]);

// Kills a strange error!
int messenger(int grid[][WIDTH], int total)
{
   return suggest(grid, total);
}

// Sees into the future!
float special(int temp_grid[][WIDTH], int moves[][2], float score[], int depth)
{   
    float current;
    int counter;
    int test_grid[HEIGHT][WIDTH] = {0};
    int marked_grid[HEIGHT][WIDTH] = {0};
    if(depth < COMPLEXITY)
    {
       for(int i = 0; i < HEIGHT; i ++)
          for(int j = i%2; j < WIDTH; j += 2)
             if(temp_grid[i][j] && ! marked_grid[i][j])
             {
                check_chain(temp_grid, marked_grid, i, j);
                if(marked_grid[i][j])
                {  
                   copy(temp_grid, test_grid);
                   make_move(test_grid, i, j);
                   current = special(test_grid, moves, score, depth + 1);
                   if(current > score[depth])
                   {
                      moves[depth][0] = i;
                      moves[depth][1] = j;
                      score[depth] = current;
                   }
                }
             }        
    }    
    else
       score[depth] = evaluate_grid(temp_grid);
    return score[depth];
}

// Suggests a move
int suggest(int grid[][WIDTH], int total)
{
   float score[8] = {0};
   int moves_list[8][2] = {0};
   special(grid, moves_list, score, temp);
   while(! score[temp])
   {
      temp ++;
      score[0] = 0;
      score[1] = 0;
      score[2] = 0;
      score[4] = 0;
      score[5] = 0;
      score[6] = 0;
      score[7] = 0;
      score[8] = 0;
      special(grid, moves_list, score, temp);
   }
   if(temp == COMPLEXITY)
   {
      for(int i = 0; i < HEIGHT; i ++)
         for(int j = 0; j < WIDTH; j ++)
            if(((grid[i][j] == grid[i][j + 1] && check_limit(i, j + 1))
            ||(grid[i][j] == grid[i][j - 1] && check_limit(i, j - 1))       
            ||(grid[i][j] == grid[i + 1][j] && check_limit(i + 1, j))
            ||(grid[i][j] == grid[i - 1][j] && check_limit(i - 1, j))) 
            && grid[i][j])
            {
               total += make_move(grid, i, j);
               cout << "Moves: (" << i << ", " << j << ") " << endl;
            }
      int final = count(grid);
      switch(final)
      {
         case 0:
            final = 5625;
            break;
         case 1:
            final = 4900;
            break;
         case 2:
            final = 4225;
            break;
         case 3:
            final = 4225;
            break;
         case 4:
            final = 3600;
            break;
         case 5:
            final = 3025;
            break;
         case 6:
            final = 2500;
            break;
         case 7:
            final = 2025;
            break;
         case 8:
            final = 2025;
            break;
         case 9:
            final = 1600;
            break;
         case 10:
            final = 1225;
            break;
         case 11:
            final = 900;
            break;
         case 12:
            final = 625;
            break;
         case 13:
            final = 625;
            break;
         case 14:
            final = 400;
            break;
         case 15:
            final = 225;
            break;
         case 16:
            final = 100;
            break;
         case 17:
            final = 100;
            break;
         case 18:
            final = 25;
            break;
      cout << "Final Score: " << (final += total) << endl;
      display(grid, 1);
      exit(0);
   }
   cout << "Moves: ";
   for(int i = temp; i < COMPLEXITY; i ++)
      {
         cout << "(" << moves_list[i][0] << ", " << moves_list[i][1] << ") ";
         total += make_move(grid, moves_list[i][0], moves_list[i][1]);
      }
   cout << endl << endl;
   cout << "Connectivity: " << evaluate_grid(grid) * 100 << "%     Score: " << total << endl;
   if(count(grid) < 80) COMPLEXITY = 6;
   if(count(grid) < 60) COMPLEXITY = 7;
   if(count(grid) < 35) COMPLEXITY = 8;
   return total;
}


// This function works out how "good" the grid is
float evaluate_grid(int temp_grid[][WIDTH])
{
   float score = 0;
   float count = 0;
   if(temp_grid[0][0])
   {
      if((temp_grid[0][0] == temp_grid[0][1])   
      || (temp_grid[0][0] == temp_grid[1][0]))
      score ++;
      count ++;
   }
   for(int j = 1; j < WIDTH - 1; j ++)
      if(temp_grid[0][j])
      {
         if((temp_grid[0][j] == temp_grid[0][j + 1])
         || (temp_grid[0][j] == temp_grid[0][j - 1])       
         || (temp_grid[0][j] == temp_grid[1][j]))
         score ++;
         count ++;
      } 
   if(temp_grid[0][WIDTH - 1])
   {
      if((temp_grid[0][WIDTH - 1] == temp_grid[0][WIDTH - 2])       
      || (temp_grid[0][WIDTH - 1] == temp_grid[1][WIDTH - 1]))
      score ++;
      count ++;
   }
   for(int i = 1; i < HEIGHT - 1; i ++)
   {
      if(temp_grid[i][0])
      {
      	 if((temp_grid[i][0] == temp_grid[i][1])   
         || (temp_grid[i][0] == temp_grid[i + 1][0])
         || (temp_grid[i][0] == temp_grid[i - 1][0]))
         score ++;
         count ++;
      }
      for(int j = 1; j < WIDTH - 1; j ++)
      	 if(temp_grid[i][j])
         {
            if((temp_grid[i][j] == temp_grid[i][j + 1])
            || (temp_grid[i][j] == temp_grid[i][j - 1])       
            || (temp_grid[i][j] == temp_grid[i + 1][j])
            || (temp_grid[i][j] == temp_grid[i - 1][j]))
            score ++;
            count ++;
         } 
      if(temp_grid[i][WIDTH - 1])
      {
         if((temp_grid[i][WIDTH - 1] == temp_grid[i][WIDTH - 2])       
         || (temp_grid[i][WIDTH - 1] == temp_grid[i + 1][WIDTH - 1])
         || (temp_grid[i][WIDTH - 1] == temp_grid[i - 1][WIDTH - 1]))
         score ++;
         count ++;
      }
   }
   if(temp_grid[HEIGHT - 1][0])
      {
         if((temp_grid[HEIGHT - 1][0] == temp_grid[HEIGHT - 1][1])   
         || (temp_grid[HEIGHT - 1][0] == temp_grid[HEIGHT - 2][0]))
         score ++;
         count ++;
      }
   for(int j = 1; j < WIDTH - 1; j ++)
      if(temp_grid[HEIGHT - 1][j])
      {
         if((temp_grid[HEIGHT - 1][j] == temp_grid[HEIGHT - 1][j + 1])
         || (temp_grid[HEIGHT - 1][j] == temp_grid[HEIGHT - 1][j - 1])       
         || (temp_grid[HEIGHT - 1][j] == temp_grid[HEIGHT - 2][j]))
         score ++;
         count ++;
      } 
   if(temp_grid[HEIGHT - 1][WIDTH - 1])
   {
      if((temp_grid[HEIGHT - 1][WIDTH - 1] == temp_grid[HEIGHT - 1][WIDTH - 2])       
      || (temp_grid[HEIGHT - 1][WIDTH - 1] == temp_grid[HEIGHT - 2][WIDTH - 1]))
      score ++;
      count ++;
   }
   return score / count;
}

// makes a move
int make_move(int grid[][WIDTH], int a, int b)
{  
   int chain_size;
   int check_grid[HEIGHT][WIDTH] = {0};
   check_chain(grid, check_grid, a, b);
   chain_size = count(check_grid);
   chain_size *= chain_size * 5;
   shift(grid, check_grid);
   return chain_size;
}

void main()
{ 
   int grid[HEIGHT][WIDTH] = {0};
   int finish = 1;
   int total = 0;
   input(grid);
   while(finish)
   {   
     total = messenger(grid, total);
     display(grid, 1);
     cout << "Finish? (0 for yes, 1 for no): ";
     cin >> finish;
     cout << endl;
   }
}

// Counts the number of non-zero elements in any array passed in
int count(int temp_grid[][WIDTH])
{
   int counter = 0;
   for(int i = 0; i < HEIGHT; i ++)
      for(int j = 0; j < WIDTH; j ++) 
         if(temp_grid[i][j]) counter ++;
   return counter;
}

// Assigns 0 to all the array members
void reset_grid(int temp_grid[][WIDTH]) 
{
   for(int i = 0; i < HEIGHT; i ++)
      for(int j = 0; j < WIDTH; j ++)
         temp_grid[i][j] = 0;
}

// This functions "drops" the blocks down into any empty spaces
void shift_down(int temp_grid[][WIDTH], int a, int b)
{
   for(int i = a; i > 0; i --)
      temp_grid[i][b] = temp_grid[i - 1][b];
   temp_grid[0][b] = 0;
}

// This function moves columns of blocks to the right when a column disappears
void shift_right(int temp_grid[][WIDTH], int a)
{
   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = a; j > 0; j --)
         temp_grid[i][j] = temp_grid[i][j - 1];
      temp_grid[i][0] = 0;
   }
}

// This functions moves the blocks when there are spaces available
void shift(int temp_grid[][WIDTH], int check_grid[][WIDTH])
{
   int column;
   for(int i = 0; i < HEIGHT; i ++)
      for(int j = 0; j < WIDTH; j ++)
         if(check_grid[i][j]) shift_down(temp_grid, i, j);
   for(int i = 0; i < WIDTH; i ++)
   {
      column = 0;
      for(int j = 0; j < HEIGHT; j ++)
         if(temp_grid[j][i])
         {
            column = 1; 
            break;
         }
      if(! column) shift_right(temp_grid, i);
   }  
}

// This function checks if the co-ordinates are within the grid limits
bool check_limit(int a, int b)
{
   return !(a < 0 || b < 0 || a >= HEIGHT || b >= WIDTH);
}

// This function displays any 2-dimensional array passed into it
void display(int temp_grid[][WIDTH], int option)
{
   cout << endl;
   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         if(option) 
         {
            if(! temp_grid[i][j]) cout << "  ";
            else cout << " " << temp_grid[i][j];
         }
         else cout << temp_grid[i][j];
      }
      cout << endl;
   } 
   cout << endl;
}

// Finds the chain which includes the "block passed in"
void check_chain(int grid[][WIDTH], int check_grid[][WIDTH], int a, int b)
{
   if(! check_grid[a][b] && grid[a][b])
   {
      if(grid[a][b] == grid[a][b + 1] && check_limit(a, b + 1))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a, b + 1);
      }
      if(grid[a][b] == grid[a + 1][b] && check_limit(a + 1, b))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a + 1, b);
      }
      if(grid[a][b] == grid[a][b - 1] && check_limit(a, b - 1))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a, b - 1);
      }
      if(grid[a][b] == grid[a - 1][b] && check_limit(a - 1, b))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a - 1, b);
      }
   }     
}

// Takes in user input of the grid
void input(int grid[][WIDTH])
{
   cout << endl;
   cout << "Enter the blocks in one line at a time followed by enter." << endl;
   cout << "Dont put spaces in, and of course use numbers 1-5 to represent the blocks!";
   cout << endl << endl;
   char data[HEIGHT][WIDTH + 1];   
   for(int i = 0; i < HEIGHT; i ++)
   {
      cin >> data[i];
      for(int j = 0; j < WIDTH; j ++)
      	grid[i][j] = data[i][j] - 48;
   }
   cout << endl;
}

// Copys the first grid to the second grid
void copy(int temp_grid[][WIDTH], int grid[][WIDTH])
{
   for(int i = 0; i < HEIGHT; i ++)
      for(int j = 0; j < WIDTH; j ++)
         grid[i][j] = temp_grid[i][j];
}
