#include <iostream.h>  
#include <stdlib.h> 
#include <math.h>





const int HEIGHT = 9;
const int WIDTH = 14; 





int count(int temp_grid[][WIDTH]);
void reset_grid(int temp_grid[][WIDTH]);
void shift_down(int temp_grid[][WIDTH], int a, int b);
void shift_right(int temp_grid[][WIDTH], int a);
void shift(int temp_grid[][WIDTH]);
void display(int temp_grid[][WIDTH], int option);
void copy(int temp_grid[][WIDTH], int grid[][WIDTH]);
float evaluate_grid(int temp_grid[][WIDTH]);
float evaluate_grid_SD(int temp_grid[][WIDTH]);
float special(int temp_grid[][WIDTH], int depth, int a, int b);


bool check_limit(int a, int b);


void suggest(int grid[][WIDTH]);
void input(int grid[][WIDTH]);
void check_chain(int grid[][WIDTH], int check_grid[][WIDTH], int a, int b);
float evaluate_move(int grid[][WIDTH], int a, int b, int select);





// Sees into the future!

float special(int temp_grid[][WIDTH], int depth, int a, int b)
{
    depth ++;
    float score = 0;
    float current = 0;
    int counter;

    int test_grid[HEIGHT][WIDTH] = {0};
    copy(temp_grid, test_grid);


    if(depth < 4)
    {
       for(int i = 0; i < HEIGHT; i ++)
       {
          for(int j = div(i, 2).rem; j < WIDTH; j += 2)
          {
             copy(temp_grid, test_grid);

             counter = count(test_grid);
             evaluate_move(test_grid, i, j, 1);
             

             if(counter != count(test_grid))
             {
                current = special(test_grid, depth, i, j);
             }

             if(current >= score && current != 0)
             {
                score = current;
             }
          }
       }          
    }

    else
    {
       evaluate_move(test_grid, a, b, 1);

       if(count(test_grid) != 0)
       {
          score = evaluate_grid(test_grid) / count(test_grid);
       }
      
       else
       {
          score = 1;
       }
    }

    return score;
}





// Suggests a move

void suggest(int grid[][WIDTH])
{
   float current, best = 0;
   int best_i = 0, best_j = 0;


   int choice, end;


   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      { 
          current = evaluate_move(grid, i, j, 0);
          

          if(current >= best && current != 0)
          {
             best = current;
             
             best_i = i;
             best_j = j;
          }
      }
   }

   //cout << special(grid, 0, 0, 0);




   if(best == 0)
   { 
      cout << endl;
      cout << "One or no moves remaining, shall I finish the game? 0 for yes, 1 for no: ";
      cin >> end; 


      if(end == 0)
      {
         for(int i = 0; i < HEIGHT; i ++)
         {
            for(int j = 0; j < WIDTH; j ++)
            { 
               evaluate_move(grid, i, j, 1);
            }
         }

         
         cout << endl << "Finished!" << endl << endl;
         exit(0);
      }      
   }


   else
   {   
      cout << endl << "Suggested move: " << "(" << best_i << "," << best_j << ")" << endl;



      cout << endl << "Select 1 to use suggestion, or 2 to enter own co-ordinates: ";
      cin >> choice;


      if(choice == 2)
      {
         cin >> best_i >> best_j;
      }

      evaluate_move(grid, best_i, best_j, 1);
   }
}





// This function is like evaluate_grid but with Standard Deviation

// Basically it gives a slightly higher score if the number of blocks of
// each colour are similar

float evaluate_grid_SD(int temp_grid[][WIDTH])
{
   int frequency[6] = {0};

   int marked_grid[HEIGHT][WIDTH] = {0};
   float score = 0;
   
   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         frequency[(temp_grid[i][j])] ++;



	 if(temp_grid[i][j] != 0)
         {
            if(temp_grid[i][j] == temp_grid[i][j + 1] && check_limit(i, j + 1))
            {
               marked_grid[i][j] = temp_grid[i][j];
               marked_grid[i][j + 1] = temp_grid[i][j];
            }
         
            if(temp_grid[i][j] == temp_grid[i + 1][j] && check_limit(i + 1, j))
            {
               marked_grid[i][j] = temp_grid[i][j];
               marked_grid[i + 1][j] = temp_grid[i][j];
            }

            if(marked_grid[i][j] != 0)
            {
               score ++;
            }
         }
      }
   } 
  


   

   float average;
   float total;
   float sq_average;
   float deviation;

   for(int i = 1; i < 6; i ++)
   {
      total += frequency[i];
      sq_average += frequency[i] * frequency[i];
   }

   average = total / 5;
   sq_average = sqrt(sq_average / 5);

   deviation = sq_average / average;
   

   return (score / (total * deviation));
}





// This function calculates "how good" the grid is at any time

float evaluate_grid(int temp_grid[][WIDTH])
{
   int marked_grid[HEIGHT][WIDTH] = {0};
   float score = 0;
   
   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {

	 if(temp_grid[i][j] != 0)
         {
            if(temp_grid[i][j] == temp_grid[i][j + 1] && check_limit(i, j + 1))
            {
               marked_grid[i][j] = temp_grid[i][j];
               marked_grid[i][j + 1] = temp_grid[i][j];
            }
         
            if(temp_grid[i][j] == temp_grid[i + 1][j] && check_limit(i + 1, j))
            {
               marked_grid[i][j] = temp_grid[i][j];
               marked_grid[i + 1][j] = temp_grid[i][j];
            }

            if(marked_grid[i][j] != 0)
            {
               score ++;
            }
         }
      }
   } 
   

   return score;
}





// Calculates how good a specific move is // needs updating perhaps

float evaluate_move(int grid[][WIDTH], int a, int b, int select)
{
   int check_grid[HEIGHT][WIDTH] = {0};

   check_chain(grid, check_grid, a, b);


   int comp_grid[HEIGHT][WIDTH] = {0};

   for(int i = 0; i < HEIGHT; i ++)
   { 
      for(int j = 0; j < WIDTH; j ++)
      {   
         if(check_grid[i][j] == 1)
         {
            comp_grid[i][j] = 0;
         }

         else
         {
            comp_grid[i][j] = grid[i][j];
         }
      }
   }

   shift(comp_grid);


   if(select == 1)
   {  

      for(int i = 0; i < HEIGHT; i ++)
      { 
         for(int j = 0; j < WIDTH; j ++)
         {   
            grid[i][j] = comp_grid[i][j];
         }
      }
   }




   int same = 1;
   
   for(int m = 0; m < HEIGHT; m ++)
   {
      for(int n = 0; n < WIDTH; n ++)
      {
         if(check_grid[m][n] == 1)
         {
            same = 0;
            n = WIDTH;
            m = HEIGHT;
         }           
      }
   }
 


        
   if(same == 1)
   {
      return 0;
   }
 
   else
   {
      return evaluate_grid_SD(comp_grid);
   }
}




 
int main()
{ 
   int grid[HEIGHT][WIDTH] = {0};
   int test_grid[HEIGHT][WIDTH] = {0};
   
   input(test_grid);
   copy(test_grid, grid);


   int test = 0;
   int end = 1;





   while(end)
   {
   

      cout << "What would you like to do next? "<< "(the current score is: ";
      cout << evaluate_grid(test_grid) << "/"<< count(test_grid) << ")"<< endl << endl;
      cout << "Enter 1 to continue testing;" << endl;
      cout << "Enter 2 to move back to the master grid;" << endl;
      cout << "Enter 3 to update the master grid: ";
     
      cin >> test;




      if(test == 1)
      {
         suggest(test_grid);
         display(test_grid, 0);
      }

      else if(test == 2)
      {
         copy(grid, test_grid);
         display(test_grid, 0);
      }

      else if(test == 3)
      {
         copy(test_grid, grid);
         display(test_grid, 0);
      }


      cout << endl;
   }


   cout << "Just one choice left, so the rest is up to you! Sorry I messed up!";
   cout << endl << endl;

   return 0;
}





// Counts the number of non-zero elements in any array passed in

int count(int temp_grid[][WIDTH])
{
   int counter = 0;

   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         if(temp_grid[i][j] != 0)
         {
            counter ++;
         }
      }
   }

   return counter;
}





// Assigns 0 to all the array members

void reset_grid(int temp_grid[][WIDTH])
{
   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         temp_grid[i][j] = 0;
      }
   }
}





// This functions "drops" the blocks down into any empty spaces

void shift_down(int temp_grid[][WIDTH], int a, int b)
{
   for(int i = a; i > 0; i --)
   {
      temp_grid[i][b] = temp_grid[i - 1][b];
   }

   temp_grid[0][b] = 0;
}





// This function moves columns of blocks to the right when a column disappears

void shift_right(int temp_grid[][WIDTH], int a)
{
  for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = a; j > 0; j --)
      {
         temp_grid[i][j] = temp_grid[i][j - 1];
      }
    
      temp_grid[i][0] = 0;
   }
}





// This functions moves the blocks when there are spaces available

void shift(int temp_grid[][WIDTH])
{
   int column;

   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         if(temp_grid[i][j] == 0)
         {
            shift_down(temp_grid, i, j);
         }
      }
   }

   for(int i = 0; i < WIDTH; i ++)
   {
      column = 0;

      for(int j = 0; j < HEIGHT; j ++)
      {
         if(temp_grid[j][i] != 0)
         {
            column = 1; 
         }
      }

      if(column == 0)
      {
        shift_right(temp_grid, i);
      }
   }  
}






// This function checks if the co-ordinates are within the grid limits

bool check_limit(int a, int b)
{
   if(a < 0 || b < 0 || a >= HEIGHT || b >= WIDTH)
   {
      return false;
   }

   else
   {
      return true;
   }
}





// This function displays any 2-dimensional array passed into it

void display(int temp_grid[][WIDTH], int option)
{
   cout << endl;

   for(int i = 0; i < HEIGHT; i ++)
   {
      for(int j = 0; j < WIDTH; j ++)
      {
         if(option == 1)
         {
            if(temp_grid[i][j] == 0)
            { 
               cout << " ";
            }

            else
            {
               cout << temp_grid[i][j];
            }
         }
         
         else
         {
            cout << temp_grid[i][j];
         }
      }

      cout << endl;
   } 

   cout << endl;
}





// Finds the chain which includes the "block passed in"

void check_chain(int grid[][WIDTH], int check_grid[][WIDTH], int a, int b)
{
   if(check_grid[a][b] == 0 && grid[a][b] != 0)
   {
      if(grid[a][b] == grid[a][b + 1] && check_limit(a, b + 1))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a, b + 1);
      }

      if(grid[a][b] == grid[a + 1][b] && check_limit(a + 1, b))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a + 1, b);
      }

      if(grid[a][b] == grid[a][b - 1] && check_limit(a, b - 1))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a, b - 1);
      }

      if(grid[a][b] == grid[a - 1][b] && check_limit(a - 1, b))
      {
         check_grid[a][b] = 1;
         check_chain(grid, check_grid, a - 1, b);
      }
   }     
}






// Takes in user input of the grid

void input(int grid[][WIDTH])
{
   cout << endl;
   cout << "Enter the blocks in one line at a time followed by enter." << endl;
   cout << "Dont put spaces in, and of course use numbers 1-5 to represent the blocks!";
   cout << endl << endl;





   char data[HEIGHT][WIDTH + 1];   

   for(int i = 0; i < HEIGHT; i ++)
   {
      cin >> data[i];
      
      for(int j = 0; j < WIDTH; j ++)
      {
	 grid[i][j] = data[i][j] - 48;
      }
   }
   
   cout << endl;
}





// Copys the first grid to the second grid

void copy(int temp_grid[][WIDTH], int grid[][WIDTH])
{
   for(int i = 0; i < HEIGHT; i ++)
   {  
      for(int j = 0; j < WIDTH; j ++)
      {
         grid[i][j] = temp_grid[i][j];
      }
   }
}